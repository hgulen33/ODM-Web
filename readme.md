<p align="center"><a href="https://nevsehirodm.meb.gov.tr"><img src="https://raw.githubusercontent.com/electropsycho/ODM.Web/master/resources/images/Logo.png"></a></p>
    
# Ölçme Değerlendirme Merkezleri Web Yazılımı(ODM.Web)

Bu yazılım ölçme değerlendirme merkezlerinin işlerini kolaylaştırmak amacıyla PHP/Laravel framework'ü kullanılarak web tabanlı ve açık kaynak olarak geliştirilmektedir.
Yazılım içerisinde kullanıcı yönetimi, sınav yönetimi, soru yönetimi gibi moduller bulunmaktadır.
Bütün modüller dokümantasyon kısmında açıklanacaktır.

## Dökümanlar

Dökümanlar en kısa sürede eklenecektir. [Hazırlanıyor]().

## Nasıl katkıda bulunurum?

Yardım etmeyi düşündünüz için teşekkür ederiz detaylar yakında gelecek...

## Lisans

Bu yazılım Elektrik Elektronik Teknolojileri Alanı/Elektrik Öğretmeni Hakan GÜLEN tarafından geliştirilmiş olup geliştirilen bütün kaynak kodlar 
ticari amaçlarla kullanımı kısıtlanan Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) ile lisanslanmıştır.
Ayrıntılı lisans bilgisi için [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.tr) sayfasını ziyaret edebilirsiniz.2019

## Yararlanılan yazılım framework ya da kütüphaneleri

* [Laravel 5.8](https://laravel.com/docs/5.8/)
* [Vue.js](https://vuejs.org/v2/guide/)
* [Laravel Mix](https://laravel-mix.com/)
* [TymonJWT](https://github.com/tymondesigns/jwt-auth)
