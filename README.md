# Başlangıç

Bu yazılım ölçme değerlendirme merkezlerinin işlerini kolaylaştırmak amacıyla PHP/Laravel framework'ü kullanılarak web tabanlı ve açık kaynak olarak geliştirilmektedir. Yazılım içerisinde kullanıcı yönetimi, sınav yönetimi, soru yönetimi gibi moduller bulunmaktadır. Bütün modüller dokümantasyon kısmında açıklanacaktır.

## Dökümanlar

Dökümanlar için GitBook [bağlantısı](https://hgulen33.gitbook.io/odm-web/).

## Nasıl katkıda bulunurum?

Öncelikler yazılım geliştirme sürecinin bir organizmaya benzediği düşünülmelidir. Program yaşam döngüsü içinde hatalar ve eksikliler elbette olasıdır. Yardımlarınızla bunları lehimize çevirebiliriz. Tekrardan yardım etmeyi düşündüğünüz için teşekkür ederiz. Herhangi bir geliştirme isteği ya da sorun bildirimini [Yeni Sorun \(New Issue\)](https://github.com/electropsycho/ODM.Web/issues/new) bağlantısından yazarak ekleyebilir geliştirme isteğinde bulunabilir ya da projeyi fork ederek kendi dalınız \(branch\) da geliştirmeye devam edebilirsiniz.

## Lisans

Bu yazılım Elektrik Elektronik Teknolojileri Alanı/Elektrik Öğretmeni Hakan GÜLEN tarafından geliştirilmiş olup geliştirilen bütün kaynak kodlar ticari amaçlarla kullanımı kısıtlanan Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International \(CC BY-NC-SA 4.0\) ile lisanslanmıştır. Ayrıntılı lisans bilgisi için [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.tr) sayfasını ziyaret edebilirsiniz.2019

## Yararlanılan yazılım framework ya da kütüphaneleri

* [Laravel 5.8](https://laravel.com/docs/5.8/)
* [Vue.js](https://vuejs.org/v2/guide/)
* [Laravel Mix](https://laravel-mix.com/)
* [TymonJWT](https://github.com/tymondesigns/jwt-auth)

