// const LOGIN = 'login';
export const LOGIN_SUCCESSFUL = 'loginSuccessful'
export const LOGOUT = 'logout'
export const LOGIN_ERROR = 'loginError'
export const IS_SIGNING_IN = 'isSigningIn'
