<?php


namespace App\Http\Controllers;

/**
 * Responsları Biraz kolaylaştırma amaçlı eklendi
 * Class ResponseHelper
 * @package App\Http\Controllers
 */
class ResponseHelper
{
  //Mesaj adları
  const MESSAGE = "message";
  const EXCEPTION = "exception";

  const EXCEPTION_MESSAGE = "Sunucu bazlı bir hata meydana geldi!";
}
